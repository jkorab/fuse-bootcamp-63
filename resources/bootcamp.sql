drop table if exists AUDIT_EVENTS;
drop user if exists FUSE;

create user FUSE password 'sparky' admin;
create table AUDIT_EVENTS (
	EVENT_ID int identity(1) primary key,
	MESSAGE text not null,
	EVENT_TS timestamp default CURRENT_TIMESTAMP(),
	LOADED boolean default FALSE
);

insert into audit_events(message) values ('something happened');
insert into audit_events(message) values ('something happened again');
insert into audit_events(message) values ('...and again');

select * from audit_events;